# Geo Wind Public-  Wind Turbines
This project is an EDA looking at wind speed data in the US and turbine location.

# Getting started
This project requires a lot of packages not commonly used. Highly recommend creating a new environment in which to run this project so that various packages don't get overriden or corrupted by some of the geodata packages.
Highly recommend using some sort of cloud computing. The data storing windspeeds is very large and hard to handle. It is raster data not polygon geodata.
Additionally, recommend going to the source to download the windspeed data I started off with: https://globalwindatlas.info/api/gis/country/USA/wind-speed/50

# Roadmap
I plan on elaborating on this project in the future by doing more overlay maps and learning more about maximum capacity for wind turbines. Also want to remove areas on the map where there is residential space to get a clearer idea of which farmland and industrial land is available.